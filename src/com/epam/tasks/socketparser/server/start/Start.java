package com.epam.tasks.socketparser.server.start;

import com.epam.tasks.socketparser.server.controller.TextServerSocket;

public class Start {

	public static void main(String[] args) {
		TextServerSocket server = new TextServerSocket();
		server.start();
	
	}

}
