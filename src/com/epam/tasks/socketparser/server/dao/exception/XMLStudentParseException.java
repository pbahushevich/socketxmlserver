package com.epam.tasks.socketparser.server.dao.exception;

public class XMLStudentParseException extends Exception {

	public XMLStudentParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public XMLStudentParseException(String message) {
		super(message);
	}

	public XMLStudentParseException(Throwable cause) {
		super(cause);
	}

}
