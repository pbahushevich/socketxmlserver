package com.epam.tasks.socketparser.server.dao;

import com.epam.tasks.socketparser.entity.Student;
import com.epam.tasks.socketparser.server.dao.exception.XMLStudentParseException;

import java.io.InputStream;
import java.util.List;

/**
 * Created by R2Cons on 25.12.2015.
 */
public interface XMLStudentParser {
     List<Student> parseInput(InputStream inputStream) throws XMLStudentParseException;
}
