package com.epam.tasks.socketparser.server.dao.parser.stax;

import com.epam.tasks.socketparser.server.dao.XMLStudentParser;
import com.epam.tasks.socketparser.server.dao.exception.XMLStudentParseException;
import com.epam.tasks.socketparser.server.logic.AttributeName;
import com.epam.tasks.socketparser.server.logic.TagName;
import com.epam.tasks.socketparser.entity.Address;
import com.epam.tasks.socketparser.entity.Student;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belarus on 13.12.2015.
 */
public class XMLStAXStudentParser implements XMLStudentParser {
    private List<Student> students = new ArrayList<>();
    private Student student= null;
    private Address address = null;
    private StringBuilder content = new StringBuilder();

    @Override
    public List<Student> parseInput(InputStream inputStream) throws XMLStudentParseException{
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader streamReader = factory.createXMLStreamReader(inputStream);
            processStream(streamReader);
            return students;
       } catch (XMLStreamException e) {
            throw new XMLStudentParseException(e);
        }
    }
    private void processStream(XMLStreamReader streamReader) throws XMLStreamException{
        TagName tagName = null;

        while(streamReader.hasNext()){
            int tagType = streamReader.next();
            switch (tagType){
                case XMLStreamConstants.START_ELEMENT:
                    tagName = TagName.getFromString(streamReader.getLocalName());
                    processElementStart(streamReader,tagName);
                    break;
                case XMLStreamConstants.CHARACTERS:
                    processContent(tagName, student, address, streamReader.getText().trim());
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    tagName = TagName.getFromString(streamReader.getLocalName());
                    processElementEnd(students, tagName);
            }
        }
    }

    private void processElementEnd(List<Student> students, TagName tagName) {
        switch (tagName){
            case ADDRESS:
                student.setAddress(address);
                address = null;
                break;
            case STUDENT:
                students.add(student);
                student = null;
                break;
        }
    }

    private void processElementStart(XMLStreamReader streamReader,TagName tagName) {
        content.setLength(0);
        switch (tagName){
            case STUDENT:
                student = new Student();
                processStudentAttributes(streamReader);
                break;
            case ADDRESS:
                address = new Address();
                break;
        }
    }

    private void processStudentAttributes(XMLStreamReader streamReader) {
        for (int i = 0; i < streamReader.getAttributeCount(); i++) {
            AttributeName attributeName = AttributeName.getFromString(streamReader.getAttributeLocalName(i));
            switch (attributeName){
                case LOGIN:
                    student.setLogin(streamReader.getAttributeValue(i));
                    break;
                case FACULTY:
                    student.setFaculty(streamReader.getAttributeValue(i));
                    break;
            }
        }
    }

    private static void processContent(TagName tagName, Student student, Address address, String content) {
        if(content.isEmpty()){
            return;
        }
        switch (tagName) {
            case NAME:
                student.setName(content);
                break;
            case TELEPHONE:
                student.setTelephone(content);
                break;
            case COUNTRY:
                address.setCountry(content);
                break;
            case CITY:
                address.setCity(content);
                break;
            case STREET:
                address.setStreet(content);
                break;
        }
    }
}
