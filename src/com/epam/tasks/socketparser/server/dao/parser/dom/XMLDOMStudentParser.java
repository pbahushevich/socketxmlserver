package com.epam.tasks.socketparser.server.dao.parser.dom;

import com.epam.tasks.socketparser.entity.Address;
import com.epam.tasks.socketparser.entity.Student;
import com.epam.tasks.socketparser.server.dao.XMLStudentParser;
import com.epam.tasks.socketparser.server.dao.exception.XMLStudentParseException;
import com.epam.tasks.socketparser.server.logic.AttributeName;
import com.epam.tasks.socketparser.server.logic.TagName;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belarus on 13.12.2015.
 */
public class XMLDOMStudentParser implements XMLStudentParser {

	@Override
	public List<Student> parseInput(InputStream inputStream) throws XMLStudentParseException {

		List<Student> result = new ArrayList<>();
		DOMParser domParser = new DOMParser();
		try {

			domParser.parse(new InputSource(inputStream));
			Element root = domParser.getDocument().getDocumentElement();
			NodeList elements = root.getElementsByTagName(TagName.STUDENT.getXmlform());
			for (int i = 0; i < elements.getLength(); i++) {
				Element element = (Element) elements.item(i);
				Student student = getStudentFromElement(element);
				result.add(student);
			}
			return result;
		} catch (SAXException | IOException e) {
			throw new XMLStudentParseException(e);
		}
	}

	private Student getStudentFromElement(Element element) {

		Student student = new Student();

		Element name = getSingleChild(element, TagName.NAME.getXmlform());
		Element telephone = getSingleChild(element, TagName.TELEPHONE.getXmlform());
		Element addressElement = getSingleChild(element, TagName.ADDRESS.getXmlform());

		student.setLogin(element.getAttribute(AttributeName.LOGIN.getXmlform()));
		student.setFaculty(element.getAttribute(AttributeName.FACULTY.getXmlform()));
		student.setName(name.getTextContent().trim());
		student.setTelephone(telephone.getTextContent().trim());

		student.setAddress(getAddressFromElement(addressElement));

		return student;
	}

	private Address getAddressFromElement(Element element) {

		Address address = new Address();

		Element country = getSingleChild(element, TagName.COUNTRY.getXmlform());
		Element city = getSingleChild(element, TagName.CITY.getXmlform());
		Element street = getSingleChild(element, TagName.STREET.getXmlform());

		address.setCountry(country.getTextContent().trim());
		address.setCity(city.getTextContent().trim());
		address.setStreet(street.getTextContent().trim());

		return address;
	}

	private Element getSingleChild(Element element, String childName) {
		NodeList elements = element.getElementsByTagName(childName);
		if (elements.getLength() > 0) {
			return (Element) elements.item(0);
		} else {
			return null;
		}
	}

}
