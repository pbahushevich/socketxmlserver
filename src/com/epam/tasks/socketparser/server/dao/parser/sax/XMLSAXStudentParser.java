package com.epam.tasks.socketparser.server.dao.parser.sax;

import com.epam.tasks.socketparser.entity.Student;
import com.epam.tasks.socketparser.server.dao.XMLStudentParser;
import com.epam.tasks.socketparser.server.dao.exception.XMLStudentParseException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Belarus on 12.12.2015.
 */
public class XMLSAXStudentParser implements XMLStudentParser {

    @Override
    public List<Student> parseInput(InputStream stream) throws  XMLStudentParseException{

        XMLReader reader;
        try {
            reader = XMLReaderFactory.createXMLReader();
            XMLStudentHandler handler  = new XMLStudentHandler();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(stream));
            return handler.getStudents();
        } catch (IOException | SAXException e) {
            throw new XMLStudentParseException(e);
        }
    }
}
