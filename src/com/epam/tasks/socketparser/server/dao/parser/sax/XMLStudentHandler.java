package com.epam.tasks.socketparser.server.dao.parser.sax;

import com.epam.tasks.socketparser.entity.Address;
import com.epam.tasks.socketparser.entity.Student;
import com.epam.tasks.socketparser.server.logic.TagName;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belarus on 12.12.2015.
 */
public class XMLStudentHandler extends DefaultHandler{

    public static final String ADDRESS_TAG_NAME = "address";
    public static final String LOGIN_ATTRIBUTE_NAME = "login";
    public static final String FACULTY_ATTRIBUTE_NAME = "faculty";
    public static final String STUDENT_TAG_NAME = "student";
    public static final String STUDENTS_TAG_NAME = "students";

    List<Student> students = new ArrayList<>();
    StringBuilder content = new StringBuilder();
    Student student = null;
    Address address = null;

    @Override
    public void startElement (String uri, String localName,
                              String qName, Attributes attributes){

            content.setLength(0);
            if(STUDENT_TAG_NAME.equals(localName)){
                student = new Student();
                student.setLogin(attributes.getValue(LOGIN_ATTRIBUTE_NAME));
                student.setFaculty(attributes.getValue(FACULTY_ATTRIBUTE_NAME));

            }else if(ADDRESS_TAG_NAME.equals(localName)){
                address = new Address();
            }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content.append(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

    	String errorMessage = "Found unexpected tag name ";
        
    	if (STUDENTS_TAG_NAME.equals(localName)) {
            return;
        }
        
    	TagName name = TagName.valueOf(localName.toUpperCase());
        switch (name){
            case NAME:
                student.setName(content.toString());
                content.setLength(0);
                break;
            case TELEPHONE:
                student.setTelephone(content.toString());
                content.setLength(0);
                break;
            case COUNTRY: address.setCountry(content.toString());
                content.setLength(0);
                break;
            case CITY:
                address.setCity(content.toString());
                content.setLength(0);
                break;
            case STREET:
                address.setStreet(content.toString());
                content.setLength(0);
                break;
            case ADDRESS:
                student.setAddress(address);
                address = null;
                break;
            case STUDENT:
                students.add(student);
                student = null;
                break;
		default:
			throw new SAXException(errorMessage+name);
        }
    }

    public List<Student> getStudents(){
        return students;
    }
}
