package com.epam.tasks.socketparser.server.dao;


import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by R2Cons on 24.12.2015.
 */
public class SimpleStudentFileInputStream extends FileInputStream{
    
	private static final String FILE_NAME = "src/com/epam/tasks/socketparser/server/resource/students_big_list.xml";
   
	public SimpleStudentFileInputStream() throws FileNotFoundException{
        super(FILE_NAME);
    }

}
