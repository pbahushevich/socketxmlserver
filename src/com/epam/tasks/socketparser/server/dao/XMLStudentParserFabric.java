package com.epam.tasks.socketparser.server.dao;

import com.epam.tasks.socketparser.exchange.PacketType;
import com.epam.tasks.socketparser.server.dao.parser.dom.XMLDOMStudentParser;
import com.epam.tasks.socketparser.server.dao.parser.sax.XMLSAXStudentParser;
import com.epam.tasks.socketparser.server.dao.parser.stax.XMLStAXStudentParser;

/**
 * Created by R2Cons on 25.12.2015.
 */
public class XMLStudentParserFabric {

    private XMLStudentParserFabric(){};

    public static XMLStudentParser getParser(PacketType packetType){

        switch (packetType){
            case PARSE_SAX:     return new XMLSAXStudentParser();
            case PARSE_STAX:    return new XMLStAXStudentParser();
            case PARSE_DOM:     return new XMLDOMStudentParser();
        }
        return null;
    }
}
