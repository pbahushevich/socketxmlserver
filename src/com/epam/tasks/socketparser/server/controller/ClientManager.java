package com.epam.tasks.socketparser.server.controller;

import com.epam.tasks.socketparser.entity.Student;
import com.epam.tasks.socketparser.exchange.Packet;
import com.epam.tasks.socketparser.exchange.PacketType;
import com.epam.tasks.socketparser.server.dao.SimpleStudentFileInputStream;
import com.epam.tasks.socketparser.server.dao.XMLStudentParser;
import com.epam.tasks.socketparser.server.dao.XMLStudentParserFabric;
import com.epam.tasks.socketparser.server.dao.exception.XMLStudentParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class ClientManager implements Runnable {

	private static final Logger LOG = LogManager.getLogger("com.epam.tasks.logger");
	private static final int ANSWER_CLIENT_WAIT_TIME = 1000;
	private Socket socket;
	private InputStream in;
	private OutputStream out;

	public ClientManager(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		String closeMessage = "Closed connection with the client: ";
		String errorMessage = "Error working with client connection";
		try {
			in = socket.getInputStream();
			out = socket.getOutputStream();

			Packet response;
			Packet request = readRequestFromClient();

			while (request != null && request.getPacketType() != PacketType.CLOSE_CONNECTION) {

				response = ProcessResponse(request);
				sendResponseToClient(response);
				request = readRequestFromClient();

			}
			sendCloseConfirmationToClient();

			System.out.println(closeMessage + socket.getLocalAddress() + ":" + socket.getLocalPort());

		} catch (IOException e) {
			LOG.error(errorMessage, e);
		}
	}

	private Packet readRequestFromClient() {

		String errorMessage = "Error parsing client request";

		try {
			int i = in.available();
			while (i == 0) {
				Thread.sleep(ANSWER_CLIENT_WAIT_TIME);
				i = in.available();
			}
			ObjectInputStream objectIn = new ObjectInputStream(in);
			Packet request = (Packet) objectIn.readObject();
			return request;
		} catch (IOException | InterruptedException | ClassNotFoundException e) {
			LOG.error(errorMessage, e);
			Packet errorPacket = new Packet(PacketType.REQUEST_ERROR);
			return errorPacket;
		}
	}

	private Packet ProcessResponse(Packet request) {

		if (request.getPacketType() == PacketType.REQUEST_ERROR) {
			return new Packet(PacketType.REQUEST_ERROR);
		}

		try {
			InputStream studentsStream = new SimpleStudentFileInputStream();
			XMLStudentParser parser = XMLStudentParserFabric.getParser(request.getPacketType());
			List<Student> students = parser.parseInput(studentsStream);
			Packet response = new Packet(students);
			return response;

		} catch (FileNotFoundException | XMLStudentParseException e) {
			LOG.error(e);
			return new Packet(PacketType.PARSE_ERROR);
		}

	}

	private void sendResponseToClient(Packet response) {
		String errorMessage = "Error creating response to client";
		ObjectOutputStream objectOut = null;
		
		try {
			objectOut = new ObjectOutputStream(out);
			objectOut.writeObject(response);
		} catch (IOException e) {
			LOG.error(errorMessage, e);
		}
	}

	private void sendCloseConfirmationToClient() {

		String errorMessage = "Error sending close confirmation to client";
		try {
			ObjectOutputStream objectOut = new ObjectOutputStream(out);
			objectOut.writeObject(new Packet(PacketType.CLOSE_CONNECTION));
		} catch (IOException e) {
			LOG.error(errorMessage, e);
		}
	}

}
