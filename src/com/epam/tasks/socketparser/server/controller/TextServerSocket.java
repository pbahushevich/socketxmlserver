package com.epam.tasks.socketparser.server.controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TextServerSocket {
	
	private static final int SERVER_PORT = 4825;
	private static final String STATUS_MESSAGE = "Server is running....";
	
	private ServerSocket server;

	public void start() {
		try {
			server = new ServerSocket(SERVER_PORT);

			System.out.println(STATUS_MESSAGE);
			while (true) {

				Socket socket = server.accept();
				Thread newClient = new Thread(new ClientManager(socket));
				newClient.start();

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (server != null) {
					server.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
