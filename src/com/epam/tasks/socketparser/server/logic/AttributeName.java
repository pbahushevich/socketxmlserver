package com.epam.tasks.socketparser.server.logic;

/**
 * Created by R2Cons on 25.12.2015.
 */
public enum AttributeName {
    LOGIN,FACULTY;

    public static AttributeName getFromString(String enumString) {

    	return AttributeName.valueOf(enumString.toUpperCase());

    }
    public String getXmlform(){
        return this.toString().toLowerCase();
    }
}
