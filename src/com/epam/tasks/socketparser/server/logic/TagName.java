package com.epam.tasks.socketparser.server.logic;

/**
 * Created by Belarus on 12.12.2015.
 */
public enum TagName {
    STUDENTS, STUDENT, NAME, TELEPHONE, ADDRESS, COUNTRY, CITY, STREET;

    public static TagName getFromString(String enumString) {

        return TagName.valueOf(enumString.toUpperCase());

    }
    public String getXmlform(){
        return this.toString().toLowerCase();
    }
}
